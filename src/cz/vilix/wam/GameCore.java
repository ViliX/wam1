package cz.vilix.wam;

import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.Random;
import cz.vilix.wam.managers.Bitmaps;

public class GameCore {
	Paint paint;
	int x, y;
	int xIncrement = (int) GameEngine.V_WIDTH/4;
	int yIncrement = (int) GameEngine.V_HEIGHT/4;
	int nDiameter = 270;
	int nStartPassTime = 50;
	int nPassTime = nStartPassTime;
	int nTimeCount = 0;
	int nRes = 1;
	int xRand, yRand;
	float xPaintBitmap, yPaintBitmap;
	int catched = 0;
	int xCatch[] = {45,405,765};
	int yCatch[] = {45+GameEngine.Y_OFFSET,405+GameEngine.Y_OFFSET,765+GameEngine.Y_OFFSET};
	int init = 0;
	int nLevel = 0;
	int nScore = 0;
	int nLevelBefore = 0;
	int nScoreIncrementForLevelUp = 25;
	
	public GameCore(Paint paint) {
		this.paint = paint;
		paint.setTextSize(60);
		paint.setARGB(255, 255, 255, 255);
	}
	
	public void draw(Canvas c) {
		nLevel = GameEngine.getLevel();
		nScore = GameEngine.getScore();
		c.drawText("Level " + nLevel, 100, 100, paint);
		c.drawText("Score " + nScore, 700, 100, paint);
		
		if(init != 0)
			if(catched == 0) c.drawBitmap(Bitmaps.circle01, xPaintBitmap, yPaintBitmap, paint);
			else c.drawBitmap(Bitmaps.circle02, xPaintBitmap, yPaintBitmap, paint);
		
		c.drawBitmap(Bitmaps.abilities001a, GameEngine.V_WIDTH/3*0+Bitmaps.abilities001a.getWidth()/6, GameEngine.V_WIDTH/3*3+Bitmaps.abilities001a.getWidth()/6+GameEngine.Y_OFFSET, paint);
		c.drawBitmap(Bitmaps.abilities002a, GameEngine.V_WIDTH/3*1+Bitmaps.abilities002a.getWidth()/6, GameEngine.V_WIDTH/3*3+Bitmaps.abilities002a.getWidth()/6+GameEngine.Y_OFFSET, paint);
		c.drawBitmap(Bitmaps.abilities003a, GameEngine.V_WIDTH/3*2+Bitmaps.abilities003a.getWidth()/6, GameEngine.V_WIDTH/3*3+Bitmaps.abilities003a.getWidth()/6+GameEngine.Y_OFFSET, paint);
	}
	
	public void update() {
		if(nTimeCount < nPassTime) nTimeCount++;
		else
		{
			init = 1;
			nTimeCount = 0;
			Random rnd = new Random();
			rnd = new Random();
			xRand = rnd.nextInt(3);
			yRand = rnd.nextInt(3);
			xPaintBitmap = GameEngine.V_WIDTH/3*xRand+Bitmaps.circle.getWidth()/6; 
			yPaintBitmap = GameEngine.V_WIDTH/3*yRand+Bitmaps.circle.getWidth()/6+GameEngine.Y_OFFSET;
			catched = 0;
		}
	}

	public void onTouch(int x, int y) {
		if (x > xCatch[xRand] && x < xCatch[xRand] + nDiameter && y > yCatch[yRand] && y < yCatch[yRand] + nDiameter) {
			catched = 1;
			nScore++;
		}
		else nScore--;
		nLevelBefore = nLevel;
		nLevel = (nScore/nScoreIncrementForLevelUp)+1;
		GameEngine.saveLevel(nLevel);
		if(nLevel > nLevelBefore)
			if(nLevel < nScoreIncrementForLevelUp) nScore+=nLevel;
			else nScore+=nScoreIncrementForLevelUp;
		GameEngine.saveScore(nScore);
		nPassTime = nStartPassTime - nLevel;		
	}
}