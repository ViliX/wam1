package cz.vilix.wam;

import android.content.SharedPreferences;

public class GameEngine {
	public static final float UPS = 1000/250;
	
	public static final int ALPHA = 255;
	public static final int RED = 120;
	public static final int GREEN = 170;
	public static final int BLUE = 65;
	
	public static float WIDTH;
	public static float HEIGHT;
	public static final float V_WIDTH = 1080;
	public static final float V_HEIGHT = 1920;
	
	public static final int Y_OFFSET = 300;
	
	public static final String PREFS_NAME = "WAM_PREF";
	private static SharedPreferences sp;
	
	public static int ability1 = 1;
	public static int ability2 = 2;
	public static int ability3 = 3;
	
	public static void initialize(SharedPreferences sp) {
		GameEngine.sp = sp;
	}
	
	public static void saveScore(int score) {
		SharedPreferences.Editor spe = sp.edit();
		spe.putInt("score", score);
		spe.commit();
	}
	
	public static void saveLevel(int lvl) {
		SharedPreferences.Editor spe = sp.edit();
		spe.putInt("level", lvl);
		spe.commit();
	}
	
	public static void saveAbility(int index, int ability) {
		SharedPreferences.Editor spe = sp.edit();
		spe.putInt("ability"+index, ability);
		spe.commit();
	}
	
	public static int getAbility(int index) { return sp.getInt("ability"+index, 1); }
	public static int getScore() { return sp.getInt("score", 0); }
	public static int getLevel() { return sp.getInt("level", 1); }
}