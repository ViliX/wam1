package cz.vilix.wam;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

public class Main extends ActionBarActivity {
	
	RenderView surfaceView;
	public static int screenWidth, screenHeight;
	public static Resources resources;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		resources = getResources();

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	
		DisplayMetrics m = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(m);
		GameEngine.WIDTH = m.widthPixels;
		GameEngine.HEIGHT= m.heightPixels;
		
		surfaceView = new RenderView(this);
		
		setContentView(surfaceView);
	}
	
	public void onResume() {
		super.onResume();
		surfaceView.resume();
	}

	public void onPause() {
		super.onPause();
		surfaceView.pause();
	}
}