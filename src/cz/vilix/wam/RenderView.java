package cz.vilix.wam;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import cz.vilix.wam.managers.Bitmaps;

public class RenderView extends SurfaceView implements Runnable {
	
	Context context;
	Thread thread = null;
	
	SurfaceHolder holder;
	Paint paint;
	boolean running = false;
	long lastTime = 0;
	GameCore core;
	Matrix matrix;
	
	public RenderView(Context context) {
		super(context);
		this.context = context;
		holder = getHolder();
		paint = new Paint();
		core = new GameCore(paint);
		Bitmaps.initialize();
		matrix = new Matrix();
		matrix.setScale(GameEngine.WIDTH/GameEngine.V_WIDTH, GameEngine.HEIGHT/GameEngine.V_HEIGHT);
	}

	public void run() {
		while (running) {
			if (!holder.getSurface().isValid())	continue;
			Canvas c = holder.lockCanvas();
			c.setMatrix(matrix);
			if(System.currentTimeMillis() - lastTime >= GameEngine.UPS) {
				lastTime = System.currentTimeMillis();
				core.update();
			}

			c.drawARGB(GameEngine.ALPHA, GameEngine.RED, GameEngine.GREEN, GameEngine.BLUE);
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					c.drawBitmap(Bitmaps.circle, GameEngine.V_WIDTH/3*i+Bitmaps.circle.getWidth()/6, GameEngine.V_WIDTH/3*j+Bitmaps.circle.getWidth()/6+GameEngine.Y_OFFSET, paint);
				}
			}
			core.draw(c);
			
			holder.unlockCanvasAndPost(c);
		}
	}
	
	public void pause() {
		running = false;
		try {
			thread.join();
		} catch (Exception e) {
		}
	}
	
	public void resume() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public boolean onTouchEvent(MotionEvent event) {
		core.onTouch((int) (event.getX()/GameEngine.WIDTH*GameEngine.V_WIDTH), (int) (event.getY()/GameEngine.HEIGHT*GameEngine.V_HEIGHT));
		return false;
	}
}