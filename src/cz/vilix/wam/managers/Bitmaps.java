package cz.vilix.wam.managers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import cz.vilix.wam.GameEngine;
import cz.vilix.wam.Main;
import cz.vilix.wam.R;

public class Bitmaps {
	public static Bitmap circle;
	public static Bitmap circle01, circle02;
	public static Bitmap abilities001a, abilities002a, abilities003a;
	public static void initialize() {
		circle = BitmapFactory.decodeResource(Main.resources, R.drawable.testcircle);
		circle = getResizedBitmap(circle, GameEngine.V_WIDTH/4, GameEngine.V_WIDTH/4);
		circle01 = BitmapFactory.decodeResource(Main.resources, R.drawable.testcircle01);
		circle01 = getResizedBitmap(circle01, GameEngine.V_WIDTH/4, GameEngine.V_WIDTH/4);
		circle02 = BitmapFactory.decodeResource(Main.resources, R.drawable.testcircle02);
		circle02 = getResizedBitmap(circle02, GameEngine.V_WIDTH/4, GameEngine.V_WIDTH/4);
		abilities001a = BitmapFactory.decodeResource(Main.resources, R.drawable.gc_abilities001a);
		abilities001a = getResizedBitmap(abilities001a, GameEngine.V_WIDTH/4, GameEngine.V_WIDTH/4);
		abilities002a = BitmapFactory.decodeResource(Main.resources, R.drawable.gc_abilities002a);
		abilities002a = getResizedBitmap(abilities002a, GameEngine.V_WIDTH/4, GameEngine.V_WIDTH/4);
		abilities003a = BitmapFactory.decodeResource(Main.resources, R.drawable.gc_abilities003a);
		abilities003a = getResizedBitmap(abilities003a, GameEngine.V_WIDTH/4, GameEngine.V_WIDTH/4);
	}
	
	public static Bitmap getResizedBitmap(Bitmap bm, float newHeight, float newWidth) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = newWidth / width;
		float scaleHeight = newHeight / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		return resizedBitmap;
	}
}