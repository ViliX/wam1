package cz.vilix.wam.menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import cz.vilix.wam.GameEngine;
import cz.vilix.wam.R;

public class Abilities extends Activity {
	
	Context context;
	TextView hAbilities, vCoins, vGems;
	Button ab1, ab2, ab3, ab4, ab5, ab6, ab7, ab8, ab9;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.abilities_layout);
		context = this;

		ab1 = (Button) findViewById(R.id.bAb1);
		ab2 = (Button) findViewById(R.id.bAb2);
		ab3 = (Button) findViewById(R.id.bAb3);
		ab4 = (Button) findViewById(R.id.bAb4);
		ab5 = (Button) findViewById(R.id.bAb5);
		ab6 = (Button) findViewById(R.id.bAb6);
		ab7 = (Button) findViewById(R.id.bAb7);
		ab8 = (Button) findViewById(R.id.bAb8);
		ab9 = (Button) findViewById(R.id.bAb9);
		ab1.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 1); }});
		ab2.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 2); }});
		ab3.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 3); }});
		ab4.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 4); }});
		ab5.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 5); }});
		ab6.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 6); }});
		ab7.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 7); }});
		ab8.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 8); }});
		ab9.setOnClickListener(new OnClickListener() { public void onClick(View v) { GameEngine.saveAbility(1, 9); }});
			
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/georgiabelle.ttf");
		
		hAbilities = (TextView) findViewById(R.id.hAbilities);
		hAbilities.setTypeface(font);
		
		vCoins = (TextView) findViewById(R.id.vTextCoins);
		vCoins.setTypeface(font);
		vCoins.setText("" + 1000);

		vGems = (TextView) findViewById(R.id.vTextGems);
		vGems.setTypeface(font);
		vGems.setText("" + 1000);
	}
}
