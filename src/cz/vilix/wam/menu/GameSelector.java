package cz.vilix.wam.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import cz.vilix.wam.Main;
import cz.vilix.wam.R;

public class GameSelector extends Activity {
	
	Button bStart;
	Context context;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selector_layout);
		context = this;
		
		bStart = (Button) findViewById(R.id.selectStart);
		
		bStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(context, Main.class);
				startActivity(intent);
			}
		});
	}
}