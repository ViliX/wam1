package cz.vilix.wam.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import cz.vilix.wam.GameEngine;
import cz.vilix.wam.R;

public class Menu extends Activity {
	
	Button bStart, bSettings, bInfo, bSoc, bAbilities, bCoins;
	Context context;
	TextView valueLvl, valueScore, textLvl, textScore;
	int width, height;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		setContentView(R.layout.menu_layout);
		context = this;
		
		SharedPreferences values = getSharedPreferences(GameEngine.PREFS_NAME, 0);
		GameEngine.initialize(values);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/georgiabelle.ttf");
		
		valueLvl = (TextView) findViewById(R.id.vTextLvl);
		valueLvl.setText(String.valueOf(GameEngine.getLevel()));
		valueLvl.setTypeface(font);
		valueLvl.setTextSize((int) (width/8));
		valueScore = (TextView) findViewById(R.id.vTextScore);
		valueScore.setText(String.valueOf(GameEngine.getScore()));
		valueScore.setTypeface(font);
		valueScore.setTextSize((int) (width/8));

		textScore = (TextView) findViewById(R.id.hTextScore);
		textScore.setTypeface(font);
		textScore.setTextSize((int) (width/10));

		textLvl = (TextView) findViewById(R.id.hTextLvl);
		textLvl.setTypeface(font);
		textLvl.setTextSize((int) (width/10));
		
		bAbilities = (Button) findViewById(R.id.menAbilities);
		bAbilities.setText(getString(R.string.menu_abilities));
		bAbilities.setTypeface(font);
		bAbilities.setTextSize((int) (width/10));

		bCoins = (Button) findViewById(R.id.menCoins);
		bCoins.setText(getString(R.string.menu_coins));
		bCoins.setTypeface(font);
		bCoins.setTextSize((int) (width/10));
		
		bStart = (Button) findViewById(R.id.menStart);
		bStart.getLayoutParams().width = (int) (width/2);
		bStart.getLayoutParams().height = (int) (width/2);
		bStart.setTypeface(font);
		bStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intentStart = new Intent(context, GameSelector.class);
				GameEngine.saveLevel(GameEngine.getLevel());
				GameEngine.saveScore(GameEngine.getScore());
				valueLvl.setText(String.valueOf(GameEngine.getLevel()));
				valueScore.setText(String.valueOf(GameEngine.getScore()));
				startActivity(intentStart);
			}
		});	

		bAbilities.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intentStart = new Intent(context, Abilities.class);
				startActivity(intentStart);
			}
		});
		
		bSettings = (Button) findViewById(R.id.menSettings);
		bSettings.getLayoutParams().width = (int) (width/11);
		bSettings.getLayoutParams().height = (int) (width/11);
		bSettings.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				GameEngine.saveLevel(1);
				GameEngine.saveScore(0);
				valueLvl.setText(String.valueOf(GameEngine.getLevel()));
				valueScore.setText(String.valueOf(GameEngine.getScore()));
			}
		});
		
		bInfo = (Button) findViewById(R.id.menInfo);
		bInfo.getLayoutParams().width = (int) (width/11);
		bInfo.getLayoutParams().height = (int) (width/11);
		
		bSoc = (Button) findViewById(R.id.menSoc);
		bSoc.getLayoutParams().width = (int) (width/11);
		bSoc.getLayoutParams().height = (int) (width/11);
	}
}